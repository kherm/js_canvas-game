var canvas = document.getElementById("myCanvas");
var ctx = canvas.getContext("2d");

let startGame = false;
var x = 250, y = 400, dir = 0;  //kayaker starting coordinates
var difficulty, numEnemies; //set difficulty
let enemiesArr = [];  //keep track of enemies
let keys = []; //multiple keydown directions

let sW = 1125, sH = 1125; //kayaker source img dimensions
let bW = 854, bH = 437; //background img dimensions
let count = 1; //timer for enemies, doubles as points
let lives = 3;  //you only get 3

//load all images
let bkgrd = new Image();
bkgrd.src = 'img/bkgrd2.png';

let kayaker = new Image();
kayaker.src = 'img/kayakerSprite.png';

let bear = new Image();
bear.src = 'img/bearSprite.png';

let log = new Image();
log.src = 'img/log.png';

let whirl = new Image();
whirl.src = 'img/whirl.png';

let crash = new Image();
crash.src = 'img/crash.png';

let skull = new Image();
skull.src = 'img/skull.png';
//and load the sound effects too
let crashSound = new sound("sound/crash.mp3");
let loseSound = new sound("sound/youlose.mp3");

function startScreen() {
  ctx.drawImage(bkgrd,0,0,bW,bH,0,0,600,600);
  ctx.fillStyle="orange";
  ctx.fillRect(150,370,80,40);
  ctx.fillRect(260,370,80,40);
  ctx.font = "30px Arial";
  ctx.fillStyle="black";
  ctx.fillText("Watch out for obstacles!", 150,250);
  ctx.fillText("If you hit one, you will", 150, 290);
  ctx.fillText("lose a life!", 150,330);
  ctx.fillText("Easy", 155,400);
  ctx.fillText("Hard", 265,400);
  ctx.fillText("Choose one to start", 150, 450);
  ctx.lineWidth=3;
  ctx.strokeRect(150,370,80,40);
  ctx.strokeRect(260,370,80,40);

  lifeManager(lives);
}
let clickStart = true;
canvas.onclick = function() {
  //choose difficulty - easy button
  if ((event.offsetX > 150 && event.offsetX < 230) && (event.offsetY > 370 && event.offsetY < 410)){
    difficulty = 120;
    numEnemies = 20;
    startGame = true;
  }
  //difficulty - hard button
  if ((event.offsetX > 260 && event.offsetX < 340) && (event.offsetY > 370 && event.offsetY < 410)){
    difficulty = 80;
    numEnemies = 15;
    startGame = true;
  }

  if (startGame === true && clickStart === true) {
    startGame = false;  //keep interval from restarting
    clickStart = false; //keep interval from restarting
    var cycle = 0;
    var sourceY = 1748, sourceEnd = 0 //background vars
    var top = 1748;
    let hereWeGo = setInterval (function() {  //game loop
      ctx.clearRect(0,0,600,600);
      //draw background
      ctx.drawImage(bkgrd,0,1748+sourceY,bW,600,0,0,600,600);
      ctx.drawImage(bkgrd,0,sourceY,bW,600,0,0,600, 600);
      //draw kayaker
      ctx.save();
      ctx.translate(x,y);
      ctx.rotate(dir * Math.PI / 180);
      ctx.drawImage(kayaker,cycle*sW,0,sW,sH,0,0,112,112);
      ctx.restore();
      //move kayaker
      addEventListener ("keydown", function(event) {
        keys = (keys || []);
        keys[event.keyCode] = true;
      })
      addEventListener ("keyup", function(event) {
        keys[event.keyCode] = false;
      })
      move();
      //draw enemies
      enemyManager();
      detectCollision();
      lifeManager(lives, hereWeGo);
      //cycling through images
      cycle = (cycle + 1) % 8;
      sourceY = Math.abs((sourceY - 14) % 1748); //keep from going negataive
      (sourceY < 15)? sourceY = top : sourceY -= 14;
      count += 1;
      document.getElementById("timer").innerHTML = "<b>Points: " + count + "</b>";
    },difficulty);
  }
}

//move kayaker
function move(event) {
  if (keys && keys[83] && y < 545) {
    y += 15; //move up
    dir = 0;
  }
  if (keys[65] && keys && x > 25) {
    x -= 15;   //move left
    dir = -24;
  }
  if (keys[68] && keys && x < 480) {
    x += 15;   //move right
    dir = 24;
  }
  if (keys[87] && keys && y > 15) {
    y -= 15;   //move down
    dir = 0;
  }
}

//draw enemies
function obstacles(enemy) {
  let rand = 40 + Math.random()*450; //random, within x range
  this.done = false;
  this.enemy = enemy;
  this.x = rand;
  this.y = 0;
  this.srcx = this.enemy.srcx;
  this.srcy = this.enemy.srcy;
  this.sprx = this.enemy.sprx;
  this.spry = this.enemy.spry;
  let cycle = 0;
  this.draw = function() {
    this.y += this.enemy.speed;
    ctx.drawImage(this.enemy.img, cycle * this.srcx,0, this.srcx, this.srcy, this.x, this.y, this.sprx, this.spry);
    cycle = (cycle + 1) % this.enemy.cycle;
  };
}

//new enemy object - image info
function em(img,srcx,srcy,sprx,spry,speed, cycle) {
  this.img = img;
  this.speed = speed;
  this.srcx = srcx;
  this.srcy = srcy;
  this.sprx = sprx;
  this.spry = spry;
  this.cycle = cycle;
}

function enemyManager() {
  //pick random obstacle
  let rand = Math.ceil((Math.random()*3));
  let obs;
  //assign obsacle to new obj
  if (rand == 1)
    obs = new em(bear,918,915,80,80,20,2);
  if (rand == 2)
    obs = new em(whirl,659,490,50,38,12,1);
  if (rand == 3)
    obs = new em(log,1871,689,133,49,8,1);
  //produce new obstacle every numEnemies/counts
  //increase number of enemies every 110 counts (next level)
  if(count % 110 !== 0) {
    if (count % numEnemies == 0)
      enemiesArr.push(new obstacles(obs));
  }
  if(count % 110 == 0)
    numEnemies -= 3;

  if(count % 110 > 100 && count % 110 < 109)
    drawNextLevel();

  //update position of enemies
  for(let i = 0; i < enemiesArr.length; i++) {
    if (enemiesArr[i].done == false)
      enemiesArr[i].draw();
  }
}

//inform user they've reached the next level
function drawNextLevel(){
  ctx.font = "50px Arial";
  ctx.fillStyle="orange";
  ctx.fillText("NEXT LEVEL", 150,250);
}

//stop drawing enemy if collision detected, decrease a life
function detectCollision(){
  for(let i = 0; i < enemiesArr.length; i++) {
    let enemy = enemiesArr[i];
    if((x + 112 > enemy.x && x < enemy.x + enemy.sprx) && (y < enemy.y + enemy.spry && y + 112 > enemy.y)){
      ctx.drawImage(crash,0,0,811,505,enemy.x,enemy.y,202,126);
      enemy.done = true;  //discontinue drawing enemy
      enemy.y = null;     //take enemy off canvas
      lives -= 1;         //1 step closer to death
      crashSound.play();
    }
  }
}
//draw number of lives left or invoke game over screen if 0 lives
function lifeManager(lives, play) {
  let drawlives = "";
  for(let i = 0; i < lives; i++){
    drawlives += '<img src="img/kayakerlife.png" width="75">';
  }
  document.getElementById("lives").innerHTML = drawlives;
  if (lives <= 0) { //game over
    document.getElementById("lives").innerHTML = '<img src="img/x.png" width="75"><img src="img/x.png" width="75"><img src="img/x.png" width="75">';
    ctx.drawImage(skull,0,0,2400,2177,x-10,y-10,200,181);
    gameOver(play);
  }
}
//game over screen, stop play
function gameOver(play){
  loseSound.play();
  clearInterval(play);
  ctx.font = "small-caps bolder 40px Arial";
  ctx.fillStyle = "black";
  ctx.strokeText("Game Over", 150,370);
  ctx.strokeText("Try Again?", 150,410);
  ctx.strokeText("Refresh to Start", 150, 450);
  ctx.fillStyle = "#e60000";
  ctx.fillText("Game Over", 150,370);
  ctx.fillText("Try Again?", 150,410);
  ctx.fillText("Refresh to Start", 150, 450);
}

function sound(src) {
  this.sound = document.createElement("audio");
  this.sound.src=src;
  this.sound.setAttribute("prelode", "auto");
  this.sound.setAttribute("controls", "none");
  this.sound.style.display = "none";
  document.body.appendChild(this.sound);
  this.play = function(){
    this.sound.play();
  }
  this.stop = function() {
    this.sound.pause();
  }
}
